#pragma once
#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include "ldeque.h"

using namespace std;

 class Graph
{
	LDeck<LDeck<char>> adjlist;

	void addEdge(char source, char dest) //Adds an edge connection to the edgelist of a node. One-way
	{
		LDeck<char> *tempdeck = getEdgeListHelp(source); //Pointer to the deck we're adjusting
		tempdeck->enqueue(dest);
	}

	LDeck<char>* getEdgeListHelp(char vertex)
	{
		int converter = vertex - 65; //-65 turns char A to 0, B to 1, etc
		Link<LDeck<char>> *ptr = adjlist.frontLink(); //Pointer to the card containing the deck we're adjusting
				
		for (int i = 0; i < converter; i++)
		{//sets the pointer to the correct card
			ptr = ptr->next;
		}
		LDeck<char> *tempdeck = &(ptr->element); //Pointer to the deck in said card
		return tempdeck; 
	}

	bool pathHelp(char source, char dest)
	{

		LDeck<char>* edges = getEdgeListHelp(source);//gets the edge list of the source node

		for (int x = 0; x < edges->length(); x++) //Works on each edge listed in the source vertex's edge list
		{
			char hold = edges->dequeue(); //Pulls out the connection
			if (hold == dest) //Found the connection
			{
				edges->enqueue(hold); //Puts the connection back in the edge list
				cout << dest << "-> " << source;
				return 1; //Return that we found it
			}

			if (pathHelp(hold, dest) == 1) //Runs the function again, with the node we pulled as the new source.
			{
				cout << "-> " << source;
				edges->enqueue(hold);//Puts the connection back in the edge list
				return 1;
			}
			edges->enqueue(hold);//Puts the connection back in the edge list
		}

		return 0;
	}
public:
	Graph(int vertices)
	{
		for (int i = 0; i < vertices; i++) //For each vertex in the graph, create a list of edges and add that to the edgelist
		{
			LDeck<char> edgeList;
			adjlist.enqueue(edgeList);
		}
	}

	~Graph()
	{

	}

	void printAdjList()
	{
		for (int i = 0; i < adjlist.length(); i++) //Operates on each vertex in the adjlist
		{
			LDeck<char> edgeList = adjlist.dequeue(); //Pulls a deck from the adjlist to print
			char head = i + 65; //+65 converts 0 to A, 1 to B, etc
			cout << endl << "Adjacency list of vertex " << head << endl;
			cout << " Head";
			for (int x = 0; x < edgeList.length(); x++)
			{
				char temp = edgeList.dequeue(); //Pulls an card from the deck
				cout << "-> " << temp; //Prints said card
				edgeList.enqueue(temp); //Puts it back in the deck
			}
			cout << endl;
			adjlist.enqueue(edgeList); //Done printing, put it back in the adjlist
		}
	}

	LDeck<char>* getEdgeList(char target)
	{
		 LDeck<char> *edgeList = getEdgeListHelp(target); //Gets a pointer to the edge list we're targeting
		 return edgeList;
	}

	void path(char source, char dest)
	{
		bool validpath = pathHelp(dest, source);
		cout << endl;
		if (validpath == 0)
		{
			cout << "No valid path found";
		}

	}
	void init() //populates the adjlist with premade information
	{
		addEdge('A', 'B');
		addEdge('B', 'A');
		addEdge('B', 'C');
		addEdge('B', 'D');
		addEdge('C', 'B');
		addEdge('C', 'E');
		addEdge('D', 'B');
		addEdge('D', 'E');
		addEdge('E', 'C');
		addEdge('E', 'D');
		addEdge('E', 'F');
		addEdge('E', 'G');
		addEdge('F', 'E');
		addEdge('G', 'E');
	}
};

#endif