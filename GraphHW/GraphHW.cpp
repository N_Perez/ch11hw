#include <iostream>
#include "Graph.h"

using namespace std;

int main()
{
	Graph graphOne(7);
	graphOne.init();
	graphOne.printAdjList();
	graphOne.path('A', 'E');
	graphOne.path('E', 'A');
	graphOne.path('A', 'F');
	graphOne.path('F', 'A');
	system("pause");
	return 0;
}